let arr = [
  	{name: 'K1', health: 100},
  	{name: 'K2', health: 100},
  	{name: 'K3', health: 100},
  	{name: 'K4', health: 100},
  	{name: 'K5', health: 100},
  	{name: 'K6', health: 100}
  ];
function knights(knight, arr) {
  	let random = Math.floor(Math.random() * (6 - 1 + 1)) + 1;
    let index = arr.findIndex(x => x.name === knight.name);
    if(knight.health >= random) {
      	arr[index].health = arr[index].health - random;
    	if(index < arr.length-1) {
      	console.log(`${knight.name} was hit by ${arr[index+1].name}`);
      	knights(arr[index+1], arr);
      } else {
      	console.log(`${knight.name} was hit by ${arr[0].name}`);
        knights(arr[0], arr);
      }
    } else {
    	console.log(`${knight.name} dies`);
      arr[index].health = 0;
      let filter = arr.filter(e => e.health > 0);
      if(filter.length > 1) {
      	knights(filter[0], filter);
      } else {
      	console.log(`${filter[0].name} wins`);
      }
    }
}

knights(arr[0], arr);